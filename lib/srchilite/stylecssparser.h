/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_STYLECSSSC_LIB_SRCHILITE_STYLECSSPARSER_H_INCLUDED
# define YY_STYLECSSSC_LIB_SRCHILITE_STYLECSSPARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int stylecsssc_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    BOLD = 258,                    /* BOLD  */
    ITALICS = 259,                 /* ITALICS  */
    UNDERLINE = 260,               /* UNDERLINE  */
    FIXED = 261,                   /* FIXED  */
    NOTFIXED = 262,                /* NOTFIXED  */
    NOREF = 263,                   /* NOREF  */
    KEY = 264,                     /* KEY  */
    COLOR = 265,                   /* COLOR  */
    BG_COLOR = 266,                /* BG_COLOR  */
    STRINGDEF = 267,               /* STRINGDEF  */
    BG_STRINGDEF = 268             /* BG_STRINGDEF  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define BOLD 258
#define ITALICS 259
#define UNDERLINE 260
#define FIXED 261
#define NOTFIXED 262
#define NOREF 263
#define KEY 264
#define COLOR 265
#define BG_COLOR 266
#define STRINGDEF 267
#define BG_STRINGDEF 268

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 74 "../../../lib/srchilite/stylecssparser.yy"

  int tok ; /* command */
  const std::string * string ; /* string : id, ... */
  srchilite::StyleConstant flag ;
  srchilite::StyleConstants *styleconstants;
  srchilite::KeyList *keylist;

#line 101 "../../../lib/srchilite/stylecssparser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE stylecsssc_lval;


int stylecsssc_parse (void);


#endif /* !YY_STYLECSSSC_LIB_SRCHILITE_STYLECSSPARSER_H_INCLUDED  */
