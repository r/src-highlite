/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_LANGDEF_LIB_SRCHILITE_LANGDEFPARSER_H_INCLUDED
# define YY_LANGDEF_LIB_SRCHILITE_LANGDEFPARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int langdef_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    BEGIN_T = 258,                 /* BEGIN_T  */
    END_T = 259,                   /* END_T  */
    ENVIRONMENT_T = 260,           /* ENVIRONMENT_T  */
    STATE_T = 261,                 /* STATE_T  */
    MULTILINE_T = 262,             /* MULTILINE_T  */
    DELIM_T = 263,                 /* DELIM_T  */
    START_T = 264,                 /* START_T  */
    ESCAPE_T = 265,                /* ESCAPE_T  */
    NESTED_T = 266,                /* NESTED_T  */
    EXIT_ALL = 267,                /* EXIT_ALL  */
    EXIT_T = 268,                  /* EXIT_T  */
    VARDEF_T = 269,                /* VARDEF_T  */
    REDEF_T = 270,                 /* REDEF_T  */
    SUBST_T = 271,                 /* SUBST_T  */
    NONSENSITIVE_T = 272,          /* NONSENSITIVE_T  */
    WRONG_BACKREFERENCE = 273,     /* WRONG_BACKREFERENCE  */
    LEVEL = 274,                   /* LEVEL  */
    KEY = 275,                     /* KEY  */
    STRINGDEF = 276,               /* STRINGDEF  */
    REGEXPNOPREPROC = 277,         /* REGEXPNOPREPROC  */
    VARUSE = 278,                  /* VARUSE  */
    BACKREFVAR = 279,              /* BACKREFVAR  */
    WRONG_INCLUDE_FILE = 280,      /* WRONG_INCLUDE_FILE  */
    REGEXPDEF = 281                /* REGEXPDEF  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define BEGIN_T 258
#define END_T 259
#define ENVIRONMENT_T 260
#define STATE_T 261
#define MULTILINE_T 262
#define DELIM_T 263
#define START_T 264
#define ESCAPE_T 265
#define NESTED_T 266
#define EXIT_ALL 267
#define EXIT_T 268
#define VARDEF_T 269
#define REDEF_T 270
#define SUBST_T 271
#define NONSENSITIVE_T 272
#define WRONG_BACKREFERENCE 273
#define LEVEL 274
#define KEY 275
#define STRINGDEF 276
#define REGEXPNOPREPROC 277
#define VARUSE 278
#define BACKREFVAR 279
#define WRONG_INCLUDE_FILE 280
#define REGEXPDEF 281

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 95 "../../../lib/srchilite/langdefparser.yy"

  int tok ; /* command */
  bool booloption ;
  const std::string * string ; /* string : id, ... */
  class srchilite::StringDef *stringdef;
  class StringDefs *stringdefs;
  class LangElem *langelem;
  class StateLangElem *statelangelem;
  class StateStartLangElem *statestartlangelem;
  class LangElems *langelems;
  class NamedSubExpsLangElem *namedsubexpslangelem;
  struct Key *key;
  struct ElementNamesList *keys;
  int flag ;
  unsigned int level;

#line 136 "../../../lib/srchilite/langdefparser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE langdef_lval;
extern YYLTYPE langdef_lloc;

int langdef_parse (void);


#endif /* !YY_LANGDEF_LIB_SRCHILITE_LANGDEFPARSER_H_INCLUDED  */
