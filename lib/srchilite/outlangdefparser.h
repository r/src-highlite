/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_OUTLANGDEF_LIB_SRCHILITE_OUTLANGDEFPARSER_H_INCLUDED
# define YY_OUTLANGDEF_LIB_SRCHILITE_OUTLANGDEFPARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int outlangdef_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    BEGIN_T = 258,                 /* BEGIN_T  */
    END_T = 259,                   /* END_T  */
    DOC_TEMPLATE_T = 260,          /* DOC_TEMPLATE_T  */
    NODOC_TEMPLATE_T = 261,        /* NODOC_TEMPLATE_T  */
    STYLE_TEMPLATE_T = 262,        /* STYLE_TEMPLATE_T  */
    STYLE_SEPARATOR_T = 263,       /* STYLE_SEPARATOR_T  */
    BOLD_T = 264,                  /* BOLD_T  */
    ITALICS_T = 265,               /* ITALICS_T  */
    UNDERLINE_T = 266,             /* UNDERLINE_T  */
    COLOR_T = 267,                 /* COLOR_T  */
    BG_COLOR_T = 268,              /* BG_COLOR_T  */
    FIXED_T = 269,                 /* FIXED_T  */
    NOTFIXED_T = 270,              /* NOTFIXED_T  */
    COLORMAP_T = 271,              /* COLORMAP_T  */
    DEFAULT_T = 272,               /* DEFAULT_T  */
    ONESTYLE_T = 273,              /* ONESTYLE_T  */
    TRANSLATIONS_T = 274,          /* TRANSLATIONS_T  */
    EXTENSION_T = 275,             /* EXTENSION_T  */
    ANCHOR_T = 276,                /* ANCHOR_T  */
    REFERENCE_T = 277,             /* REFERENCE_T  */
    INLINE_REFERENCE_T = 278,      /* INLINE_REFERENCE_T  */
    POSTLINE_REFERENCE_T = 279,    /* POSTLINE_REFERENCE_T  */
    POSTDOC_REFERENCE_T = 280,     /* POSTDOC_REFERENCE_T  */
    KEY = 281,                     /* KEY  */
    STRINGDEF = 282,               /* STRINGDEF  */
    REGEXDEF = 283,                /* REGEXDEF  */
    LINE_PREFIX_T = 284,           /* LINE_PREFIX_T  */
    LINENUM_T = 285,               /* LINENUM_T  */
    WRONG_INCLUDE_FILE = 286       /* WRONG_INCLUDE_FILE  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define BEGIN_T 258
#define END_T 259
#define DOC_TEMPLATE_T 260
#define NODOC_TEMPLATE_T 261
#define STYLE_TEMPLATE_T 262
#define STYLE_SEPARATOR_T 263
#define BOLD_T 264
#define ITALICS_T 265
#define UNDERLINE_T 266
#define COLOR_T 267
#define BG_COLOR_T 268
#define FIXED_T 269
#define NOTFIXED_T 270
#define COLORMAP_T 271
#define DEFAULT_T 272
#define ONESTYLE_T 273
#define TRANSLATIONS_T 274
#define EXTENSION_T 275
#define ANCHOR_T 276
#define REFERENCE_T 277
#define INLINE_REFERENCE_T 278
#define POSTLINE_REFERENCE_T 279
#define POSTDOC_REFERENCE_T 280
#define KEY 281
#define STRINGDEF 282
#define REGEXDEF 283
#define LINE_PREFIX_T 284
#define LINENUM_T 285
#define WRONG_INCLUDE_FILE 286

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 62 "../../../lib/srchilite/outlangdefparser.yy"

  int tok ; /* command */
  bool booloption ;
  const std::string * string ; /* string : id, ... */
  int flag ;

#line 136 "../../../lib/srchilite/outlangdefparser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE outlangdef_lval;


int outlangdef_parse (void);


#endif /* !YY_OUTLANGDEF_LIB_SRCHILITE_OUTLANGDEFPARSER_H_INCLUDED  */
